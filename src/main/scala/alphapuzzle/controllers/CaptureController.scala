package alphapuzzle.controllers

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.model.{MediaType, MediaTypes, StatusCodes}
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import org.apache.pekko.stream.scaladsl.{Flow, Keep, Source}
import org.apache.pekko.util.ByteString
import alphapuzzle.crop.{CropRectangle, CroppingService}
import alphapuzzle.grid.{GridEntry, GridService}
import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import io.circe.{Encoder, Json}
import uk.co.unclealex.aog.RouteProvider
import uk.co.unclealex.aog.directives.SourceDirectives
import uk.co.unclealex.aog.security.Security
import uk.co.unclealex.aog.directives.JsonIsFlattenable.given
import io.circe.syntax.*

import scala.concurrent.ExecutionContext

class CaptureController(
    val security: Security,
    val croppingService: CroppingService,
    val gridService: GridService
)(using actorSystem: ActorSystem, ec: ExecutionContext)
    extends RouteProvider
    with Directives
    with FailFastCirceSupport
    with SourceDirectives:

  import security.Directives._

  private val VALID_MEDIA_TYPES: Seq[MediaType] =
    Seq(MediaTypes.`image/jpeg`, MediaTypes.`image/png`)

  private def image[R: Encoder](
      flow: Flow[ByteString, R, NotUsed],
      unmarshaller: Source[R, NotUsed] => Route
  ): Route =
    fileUpload("image") { case (metadata, byteSource) =>
      if (VALID_MEDIA_TYPES.contains(metadata.contentType.mediaType))
        unmarshaller(byteSource.viaMat(flow)(Keep.right))
      else
        complete(
          StatusCodes.BadRequest,
          s"${metadata.contentType.mediaType} is not supported."
        )
    }

  override val route: Route =
    post:
      concat(
        path("crop"):
          requireUser0:
            image[CropRectangle](croppingService.crop, sourceAsSingle)
        ,
        path("capture"):
          requireUser0:
            image[Json](
              gridService.findGridEntries.map(_.asJson),
              sourceAsSequence
            )
      )
