package alphapuzzle.controllers

import org.apache.pekko.http.scaladsl.model.headers.EntityTag
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import alphapuzzle.dictionary.Dictionary
import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import io.circe.Json
import io.circe.syntax.*
import uk.co.unclealex.aog.RouteProvider

class DictionaryController(dictionary: Dictionary)
    extends RouteProvider
    with Directives
    with FailFastCirceSupport {

  private val dictionaryJson: Json = dictionary.asJson

  override val route: Route = {
    get {
      path("dictionary.json") {
        conditional(EntityTag(dictionary.etag)) {
          complete(dictionaryJson)
        }
      }
    }
  }
}
