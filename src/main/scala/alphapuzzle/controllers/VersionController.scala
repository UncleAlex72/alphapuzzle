package alphapuzzle.controllers

import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import io.circe.syntax.*
import uk.co.unclealex.aog.RouteProvider
import version.Version

class VersionController
    extends RouteProvider
    with FailFastCirceSupport
    with Directives:

  override val route: Route =
    get:
      path("version.json"):
        complete(Map("version" -> Version.version))
