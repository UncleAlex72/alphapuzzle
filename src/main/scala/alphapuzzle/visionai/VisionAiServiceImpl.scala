package alphapuzzle.visionai

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.util.ByteString
import com.google.cloud.vision.v1._
import com.google.protobuf.{ByteString => GoogleByteString}

import java.util.Collections
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters.CollectionHasAsScala

class VisionAiServiceImpl(
    val imageAnnotatorClient: ImageAnnotatorClient,
    val cacheService: CacheService
)(using
    ec: ExecutionContext
) extends VisionAiService:

  override def findNumbers: Flow[ByteString, Vision, NotUsed] =
    accumulateByteStrings.via(callVisionAi).via(cacheService.cache)

  private def accumulateByteStrings
      : Flow[ByteString, GoogleByteString, NotUsed] =
    Flow[ByteString].fold(GoogleByteString.empty()): (acc, byteString) =>
      acc.concat(GoogleByteString.copyFrom(byteString.toArray))

  private def callVisionAi: Flow[GoogleByteString, Vision, NotUsed] =
    Flow[GoogleByteString].mapAsync(1): bytes =>
      Future:
        val img = Image.newBuilder.setContent(bytes).build
        val feat = Feature.newBuilder.setType(Feature.Type.TEXT_DETECTION).build
        val request =
          AnnotateImageRequest.newBuilder.addFeatures(feat).setImage(img).build
        val requests = Collections.singletonList(request)
        val responses = imageAnnotatorClient
          .batchAnnotateImages(requests)
          .getResponsesList
          .asScala
          .toSeq
        Vision(
          visionAiResponse = VisionAiResponseParser(responses),
          image = bytes
        )

object VisionAiServiceImpl:

  def apply(
      cacheService: CacheService
  )(using ec: ExecutionContext): VisionAiServiceImpl =
    val imageAnnotatorSettings = ImageAnnotatorSettings.newBuilder().build
    val imageAnnotatorClient =
      ImageAnnotatorClient.create(imageAnnotatorSettings)
    new VisionAiServiceImpl(
      imageAnnotatorClient = imageAnnotatorClient,
      cacheService = cacheService
    )
