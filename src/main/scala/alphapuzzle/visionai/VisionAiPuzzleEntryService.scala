package alphapuzzle.visionai

import alphapuzzle.grid.{PuzzleEntry, PuzzleEntryService}
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.util.ByteString

class VisionAiPuzzleEntryService(visionAiService: VisionAiService)
    extends PuzzleEntryService:
  override def findPuzzleEntries: Flow[ByteString, PuzzleEntry, NotUsed] =
    visionAiService.findNumbers.mapConcat: vision =>
      vision.visionAiResponse.asPuzzleEntries
