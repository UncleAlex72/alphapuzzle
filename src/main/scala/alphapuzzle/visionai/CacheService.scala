package alphapuzzle.visionai

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow

trait CacheService:

  def cache: Flow[Vision, Vision, NotUsed]
