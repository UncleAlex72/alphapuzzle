package alphapuzzle.visionai

import com.google.protobuf.ByteString

case class Vision(visionAiResponse: VisionAiResponse, image: ByteString)
