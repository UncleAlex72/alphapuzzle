package alphapuzzle.visionai

import com.typesafe.scalalogging.StrictLogging
import io.circe.Printer
import io.circe.syntax.EncoderOps
import net.sf.jmimemagic.Magic
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.FlowShape
import org.apache.pekko.stream.scaladsl.{Broadcast, Flow, GraphDSL, Sink}

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path}
import java.util.UUID
import java.util.zip.{ZipEntry, ZipOutputStream}
import scala.jdk.StreamConverters.*
import scala.util.matching.Regex
import scala.util.{Success, Try, Using}
class CacheServiceImpl(val cacheConfiguration: CacheConfiguration)
    extends CacheService
    with StrictLogging:

  Magic.initialize()

  val maximum: Int = cacheConfiguration.maybeMaximum.getOrElse(0)
  val directory: Path = cacheConfiguration.directory

  override def cache: Flow[Vision, Vision, NotUsed] =
    if (maximum <= 0)
      Flow[Vision]
    else
      savingFlow

  private def savingFlow: Flow[Vision, Vision, NotUsed] =
    Flow.fromGraph(GraphDSL.createGraph(savingSink) {
      implicit builder: GraphDSL.Builder[NotUsed] => sink =>
        import GraphDSL.Implicits.*

        val broadcast = builder.add(Broadcast[Vision](2))

        broadcast.out(0) ~> sink
        FlowShape(broadcast.in, broadcast.out(1))
    })

  private def saveZipFile(
      image: Array[Byte],
      extension: String,
      visionAiResponse: VisionAiResponse
  ): Try[Unit] =
    val zipFile = directory.resolve(s"${UUID.randomUUID().toString}.zip")
    logger.info(s"Creating cache file $zipFile")
    Using(new ZipOutputStream(Files.newOutputStream(zipFile))):
      zipOutputStream =>
        val visionAiResponseData = Printer.spaces2
          .print(visionAiResponse.asJson)
          .getBytes(StandardCharsets.UTF_8)
        Seq(
          s"image.$extension" -> image,
          "response.json" -> visionAiResponseData
        ).foreach { case (name, data) =>
          val zipEntry: ZipEntry = new ZipEntry(name)
          zipOutputStream.putNextEntry(zipEntry)
          zipOutputStream.write(data, 0, data.length)
          zipOutputStream.closeEntry()
        }

  private def savingSink: Sink[Vision, NotUsed] =
    Sink
      .foreach[Vision]: vision =>
        val result = for
          _ <- pruneCache
          image = vision.image.toByteArray
          extension <- detectImageExtension(image)
          _ <- saveZipFile(image, extension, vision.visionAiResponse)
        yield {}
        result.get
      .mapMaterializedValue(_ => NotUsed)

  private def pruneCache: Try[Unit] =
    Using(Files.list(directory)): directoryStream =>
      val files = directoryStream
        .toScala(Vector)
        .sortBy(path => Files.getLastModifiedTime(path))
      files
        .dropRight(maximum - 1)
        .foreach: file =>
          logger.info(s"Purging cache file $file")

  private def detectImageExtension(bytes: Array[Byte]): Try[String] =
    val imageRegex: Regex = """image/(.+)""".r
    Try(Magic.getMagicMatch(bytes).getMimeType).flatMap {
      case imageRegex(extension) => Success(extension)
      case mimeType: String =>
        throw new IllegalStateException(
          s"$mimeType is not a valid mime type for an image"
        )
    }
