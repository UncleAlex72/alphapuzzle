package alphapuzzle.visionai

import java.nio.file.{Path, Paths}

case class CacheConfiguration(maybeMaximum: Option[Int], directory: Path)

object CacheConfiguration:

  import net.ceedubs.ficus.Ficus.*
  import net.ceedubs.ficus.readers.ValueReader

  given ValueReader[CacheConfiguration] = ValueReader.relative: config =>
    val maybeMaximum = config.as[Option[Int]]("maximum")
    val directory = config.as[String]("directory")
    CacheConfiguration(
      maybeMaximum = maybeMaximum,
      directory = Paths.get(directory)
    )
