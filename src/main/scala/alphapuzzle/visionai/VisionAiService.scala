package alphapuzzle.visionai

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.util.ByteString

trait VisionAiService:
  def findNumbers: Flow[ByteString, Vision, NotUsed]
