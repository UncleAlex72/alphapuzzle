package alphapuzzle.visionai

import alphapuzzle.grid.PuzzleEntry
import io.circe.Codec

import scala.util.Try

case class VisionAiResponse(textAnnotations: Seq[TextAnnotation]) derives Codec:
  def asPuzzleEntries: Seq[PuzzleEntry] =
    textAnnotations.flatMap(_.asPuzzleEntry)

case class TextAnnotation(boundingPoly: BoundingPoly, description: String)
    derives Codec:
  def parse: Option[Int] =
    for
      number <- Try(Integer.parseInt(description)).toOption
      if number >= 1 && number <= 26
    yield number

  def asPuzzleEntry: Option[PuzzleEntry] =
    val maybeNumber =
      Try(Integer.parseInt(description)).toOption.filter: value =>
        value >= 1 && value <= 26
    maybeNumber.map: number =>
      PuzzleEntry(
        column = boundingPoly.topLeft.x,
        row = boundingPoly.topLeft.y,
        number
      )

case class BoundingPoly(vertices: Seq[Vertex]) derives Codec:
  def topLeft: Vertex =
    Vertex(vertices.map(_.x).min, vertices.map(_.y).min)

case class Vertex(x: Int, y: Int) derives Codec

object VisionAiResponse:
  val empty: VisionAiResponse = VisionAiResponse(Seq.empty)
