package alphapuzzle.visionai

import com.google.cloud.vision.v1.{
  AnnotateImageResponse,
  EntityAnnotation,
  BoundingPoly as GoogleBoundingPoly,
  Vertex as GoogleVertex
}

import scala.jdk.CollectionConverters.CollectionHasAsScala

object VisionAiResponseParser:

  private implicit class JavaListHasMap[E](l: java.util.List[E]) {
    def map[F](fn: E => F): Seq[F] = {
      l.asScala.toSeq.map(fn)
    }
  }

  private def parseVertex(vertex: GoogleVertex): Vertex = {
    Vertex(x = vertex.getX, y = vertex.getY)
  }
  private def parseBoundingPoly(
      boundingPoly: GoogleBoundingPoly
  ): BoundingPoly =
    BoundingPoly(boundingPoly.getVerticesList.map(parseVertex))
  private def parseTextAnnotation(
      entityAnnotation: EntityAnnotation
  ): TextAnnotation =
    TextAnnotation(
      parseBoundingPoly(entityAnnotation.getBoundingPoly),
      entityAnnotation.getDescription
    )

  def apply(
      annotateImageResponses: Seq[AnnotateImageResponse]
  ): VisionAiResponse =
    VisionAiResponse:
      for
        annotationImageResponse <- annotateImageResponses
        textAnnotation <- annotationImageResponse.getTextAnnotationsList.asScala
      yield parseTextAnnotation(textAnnotation)
