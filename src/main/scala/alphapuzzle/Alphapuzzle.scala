package alphapuzzle

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import alphapuzzle.controllers.{
  CaptureController,
  DictionaryController,
  VersionController
}
import alphapuzzle.crop.{CroppingService, OpenCvCroppingService}
import alphapuzzle.dictionary.Dictionary
import alphapuzzle.grid.{
  GridMatcher,
  GridMatcherImpl,
  GridServiceImpl,
  PuzzleEntryService
}
import alphapuzzle.visionai.{
  CacheConfiguration,
  CacheService,
  CacheServiceImpl,
  VisionAiPuzzleEntryService,
  VisionAiService,
  VisionAiServiceImpl
}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ValueReader
import uk.co.unclealex.aog.WebApp.webApp
import uk.co.unclealex.aog.security.{Security, SecurityConfiguration}

import java.io.FileNotFoundException
import java.nio.file.{Path, Paths}
import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext

@main def Alphapuzzle(): Unit =
  val config = ConfigFactory.load()

  given actorSystem: ActorSystem = ActorSystem()
  given ec: ExecutionContext = actorSystem.dispatcher

  val dictionary =
    val tryDictionary =
      val tryResource =
        Option(
          classOf[Dictionary].getClassLoader.getResource("dictionary/words.txt")
        ).toRight(new FileNotFoundException("Cannot find the dictionary file"))
          .toTry
      tryResource.flatMap: resource =>
        Dictionary(resource)
    tryDictionary.get

  val imageExecutionContext: ExecutionContext =
    ExecutionContext.fromExecutorService(Executors.newSingleThreadExecutor())

  val croppingService: CroppingService =
    new OpenCvCroppingService()(using imageExecutionContext)

  val cacheConfiguration = config.as[CacheConfiguration]("cache")
  val cacheService: CacheService = new CacheServiceImpl(cacheConfiguration)
  val puzzleEntryService: PuzzleEntryService = new VisionAiPuzzleEntryService(
    VisionAiServiceImpl(cacheService)(using imageExecutionContext)
  )
  val gridMatcher: GridMatcher = new GridMatcherImpl

  val gridService =
    new GridServiceImpl(
      puzzleEntryService = puzzleEntryService,
      gridMatcher = gridMatcher
    )(using imageExecutionContext)

  webApp.serve: security =>
    import security.Directives.*
    val captureController: CaptureController = new CaptureController(
      security = security,
      croppingService = croppingService,
      gridService = gridService
    )
    val versionController: VersionController = new VersionController
    val dictionaryController: DictionaryController =
      new DictionaryController(dictionary = dictionary)

    basePath:
      concat(
        versionController.route,
        dictionaryController.route,
        captureController.route
      )
