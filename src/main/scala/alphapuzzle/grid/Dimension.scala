package alphapuzzle.grid

case class Dimension(width: Int, height: Int)
