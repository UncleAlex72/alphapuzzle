package alphapuzzle.grid

case class PuzzleEntry(column: Int, row: Int, number: Int)
