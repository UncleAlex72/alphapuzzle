package alphapuzzle.grid

import scala.util.Using

class GridMatcherImpl extends GridMatcher:

  private val GRID_SIZE: Int = 13
  private def closestTo(
      puzzleEntry: PuzzleEntry,
      topLefts: Seq[(Int, Int)]
  ): (Int, Int) =
    topLefts
      .map { case (x, y) =>
        val dx = Math.abs(x - puzzleEntry.column)
        val dy = Math.abs(y - puzzleEntry.row)
        val distance = Math.sqrt(dx * dx + dy * dy)
        ((x, y), distance)
      }
      .sortBy(_._2)
      .map(_._1)
      .head

  override def matchToGrid(
      puzzleEntries: Seq[PuzzleEntry],
      imageSize: Dimension
  ): Map[(Int, Int), Int] =
    val (columnWidth, rowHeight) =
      (imageSize.width / GRID_SIZE, imageSize.height / GRID_SIZE)
    val cellOrdinalByTopLeftsOfCell = (for
      c <- Range(0, GRID_SIZE)
      r <- Range(0, GRID_SIZE)
    yield (c * columnWidth, r * rowHeight) -> (c, r)).toMap
    val topLeftOfCells: Seq[(Int, Int)] = cellOrdinalByTopLeftsOfCell.keys.toSeq
    val puzzleEntriesByClosestTopLeftOfCell: Map[(Int, Int), PuzzleEntry] =
      val empty = Map.empty[(Int, Int), PuzzleEntry]
      puzzleEntries.foldLeft(empty): (map, puzzleEntry) =>
        val closest = closestTo(puzzleEntry, topLeftOfCells)
        map + (closest -> puzzleEntry)
    val empty = Map.empty[(Int, Int), Int]
    cellOrdinalByTopLeftsOfCell.foldLeft(empty) {
      case (map, ((x, y), (c, r))) =>
        val maybeNumber =
          puzzleEntriesByClosestTopLeftOfCell.get((x, y)).map(_.number)
        map ++ maybeNumber.map(number => (c, r) -> number)
    }
