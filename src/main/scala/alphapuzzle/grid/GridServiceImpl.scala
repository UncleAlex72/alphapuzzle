package alphapuzzle.grid
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.FlowShape
import org.apache.pekko.stream.scaladsl.{Broadcast, Flow, GraphDSL, Zip}
import org.apache.pekko.util.ByteString

import java.io.{ByteArrayInputStream, InputStream, SequenceInputStream}
import javax.imageio.ImageIO
import scala.concurrent.{ExecutionContext, Future}

class GridServiceImpl(
    puzzleEntryService: PuzzleEntryService,
    gridMatcher: GridMatcher
)(using ec: ExecutionContext)
    extends GridService:

  private def extractPuzzleEntries
      : Flow[ByteString, Seq[PuzzleEntry], NotUsed] =
    puzzleEntryService.findPuzzleEntries.fold(Seq.empty[PuzzleEntry])(_ :+ _)

  private def extractDimensions: Flow[ByteString, Dimension, NotUsed] =
    val empty: InputStream = new ByteArrayInputStream(Array[Byte]())
    val inputStreamFlow = Flow[ByteString]
      .map(byteString => new ByteArrayInputStream(byteString.toArray))
      .fold(empty): (acc, stream) =>
        new SequenceInputStream(acc, stream)
    inputStreamFlow.mapAsync(1): in =>
      Future:
        val image = ImageIO.read(in)
        Dimension(width = image.getWidth, height = image.getHeight)

  private def broadcastAndZip[A, B](
      fa: Flow[ByteString, A, NotUsed],
      fb: Flow[ByteString, B, NotUsed]
  ): Flow[ByteString, (A, B), NotUsed] = {
    Flow.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._
      val broadcast = b.add(Broadcast[ByteString](2))
      val zip = b.add(Zip[A, B]())

      broadcast.out(0).via(fa) ~> zip.in0
      broadcast.out(1).via(fb) ~> zip.in1

      // expose port
      FlowShape(broadcast.in, zip.out)
    })
  }
  override def findGridEntries: Flow[ByteString, GridEntry, NotUsed] =
    val puzzleEntriesAndDimensionsFlow =
      broadcastAndZip(extractPuzzleEntries, extractDimensions)
    puzzleEntriesAndDimensionsFlow
      .mapAsync(1) { case (puzzleEntries: Seq[PuzzleEntry], size: Dimension) =>
        Future:
          val empty: Seq[GridEntry] = Seq.empty
          gridMatcher
            .matchToGrid(puzzleEntries = puzzleEntries, imageSize = size)
            .foldLeft(empty) { case (acc, ((column, row), number)) =>
              acc :+ GridEntry(column = column, row = row, number = number)
            }
      }
      .mapConcat(identity)
