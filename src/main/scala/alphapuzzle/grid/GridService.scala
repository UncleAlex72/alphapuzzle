package alphapuzzle.grid

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.util.ByteString

trait GridService:
  def findGridEntries: Flow[ByteString, GridEntry, NotUsed]
