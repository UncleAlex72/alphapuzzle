package alphapuzzle.grid

trait GridMatcher:

  def matchToGrid(
      puzzleEntries: Seq[PuzzleEntry],
      imageSize: Dimension
  ): Map[(Int, Int), Int]
