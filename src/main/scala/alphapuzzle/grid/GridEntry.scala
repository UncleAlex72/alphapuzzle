package alphapuzzle.grid

import io.circe.Codec

case class GridEntry(column: Int, row: Int, number: Int) derives Codec
