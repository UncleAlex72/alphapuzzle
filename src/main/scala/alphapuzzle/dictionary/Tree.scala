package alphapuzzle.dictionary

import io.circe.syntax.EncoderOps
import io.circe.{Encoder, Json, KeyEncoder}

import scala.collection.immutable.{::, List, Nil, Set}

case class Tree[T](children: Map[T, Tree[T]]):

  def withPath(values: Iterable[T]): Tree[T] =
    _withPath(values.toList)

  private def _withPath(values: List[T]): Tree[T] =
    values match
      case Nil => this
      case v :: vs =>
        val maybeExistingChild = children.get(v)
        val child = maybeExistingChild.getOrElse(Tree())
        val newChild = child._withPath(vs)
        new Tree(children + (v -> newChild))

object Tree:

  def apply[V](): Tree[V] = new Tree[V](Map.empty)

  given treeEncoder[T](using
      keyEncoder: KeyEncoder[T],
      keyOrdering: Ordering[T]
  ): Encoder[Tree[T]] =
    (t: Tree[T]) =>
      val sortedChildren: Seq[(T, Tree[T])] = t.children.toSeq.sortBy(_._1)
      val jsonChildren: Seq[(String, Json)] = sortedChildren.map {
        case (k, t) =>
          keyEncoder(k) -> treeEncoder(using keyEncoder, keyOrdering)(t)
      }
      Json.obj(jsonChildren*)
