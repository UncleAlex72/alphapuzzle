package alphapuzzle.dictionary

import org.apache.pekko.util.ByteString
import io.circe.syntax._
import io.circe.{Encoder, KeyEncoder}

import java.net.URL
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import scala.io.Source
import scala.util.{Try, Using}

case class Dictionary(
    private[Dictionary] val tree: Tree[Char] = Tree(),
    etag: String = ""
):
  def withWord(word: String): Dictionary =
    copy(tree = tree.withPath(word.toCharArray :+ '_'))

  def withEtag(etag: String): Dictionary = copy(etag = etag)

object Dictionary:

  given Encoder[Dictionary] =
    given KeyEncoder[Char] = (ch: Char) => ch.toString
    (dictionary: Dictionary) => dictionary.tree.asJson

  private case class Builder(
      digest: MessageDigest = MessageDigest.getInstance("SHA-256"),
      dictionary: Dictionary = Dictionary()
  ):
    def addWord(word: String): Builder =
      val trimmedWord = word.toUpperCase.trim
      if (trimmedWord.isEmpty) this
      else
        digest.update(trimmedWord.getBytes(StandardCharsets.UTF_8))
        copy(dictionary = dictionary.withWord(trimmedWord))

    def build(): Dictionary =
      val etag = ByteString(digest.digest()).encodeBase64
        .decodeString(StandardCharsets.UTF_8)
      dictionary.withEtag(etag)

  def apply(source: Source): Dictionary =
    source.getLines().foldLeft(Dictionary.Builder())(_.addWord(_)).build()

  def apply(url: URL): Try[Dictionary] =
    Using(Source.fromURL(url)): source =>
      apply(source)
