package alphapuzzle.crop

import io.circe.Codec
import org.opencv.core.Point

case class Coordinate(x: Int, y: Int) derives Codec

object Coordinate:

  def apply(point: Point): Coordinate =
    def round(d: Double): Int = Math.round(d).toInt
    Coordinate(round(point.x), round(point.y))
