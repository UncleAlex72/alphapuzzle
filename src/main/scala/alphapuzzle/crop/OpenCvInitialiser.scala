package alphapuzzle.crop

import nu.pattern.OpenCV

object OpenCvInitialiser:

  def initialise(): Unit =
    OpenCV.loadLocally()
