package alphapuzzle.crop

import io.circe.Codec

case class CropRectangle(x: Int, y: Int, width: Int, height: Int) derives Codec
