package alphapuzzle.crop

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.util.ByteString

trait CroppingService:

  def crop: Flow[ByteString, CropRectangle, NotUsed]
