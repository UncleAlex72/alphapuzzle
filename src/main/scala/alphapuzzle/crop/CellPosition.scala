package alphapuzzle.crop

case class CellPosition(row: Int, column: Int)

object CellPosition:

  implicit val coordinatesOrdering: Ordering[CellPosition] = Ordering.by: c =>
    (c.row, c.column)
