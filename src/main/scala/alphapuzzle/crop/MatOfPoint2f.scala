package alphapuzzle.crop

import org.opencv.core.{MatOfPoint, Point, MatOfPoint2f => ocvMapOfPoint2f}
import org.opencv.imgproc.Imgproc

object MatOfPoint2f:

  def apply(matOfPoint: MatOfPoint): ocvMapOfPoint2f =
    new org.opencv.core.MatOfPoint2f(matOfPoint.toArray*)

  extension (mapOfPoint2f: ocvMapOfPoint2f)

    private def array: Array[Point] = mapOfPoint2f.toArray

    def length: Int = array.length

    private def transform(trn: ocvMapOfPoint2f => Unit): ocvMapOfPoint2f =
      val target = new ocvMapOfPoint2f()
      trn(target)
      target

    def arcLength(closed: Boolean): Double =
      Imgproc.arcLength(mapOfPoint2f, closed)

    def approxPolyDP(epsilon: Double, closed: Boolean): ocvMapOfPoint2f =
      transform(Imgproc.approxPolyDP(mapOfPoint2f, _, epsilon, closed))

    def contourArea(): Double =
      Imgproc.contourArea(mapOfPoint2f)

    def isContourConvex(): Boolean =
      Imgproc.isContourConvex(new MatOfPoint(array*))

    def apply(idx: Int): Point = array(idx)

    def toSeq: Seq[Point] = array.toSeq

    def angle(i1: Int, i2: Int, i3: Int): Double =
      angle(array(i1), array(i2), array(i3))

    private def angle(pt1: Point, pt2: Point, pt0: Point): Double =
      val dx1 = pt1.x - pt0.x
      val dy1 = pt1.y - pt0.y
      val dx2 = pt2.x - pt0.x
      val dy2 = pt2.y - pt0.y
      (dx1 * dx2 + dy1 * dy2) / Math.sqrt(
        (dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10
      )
