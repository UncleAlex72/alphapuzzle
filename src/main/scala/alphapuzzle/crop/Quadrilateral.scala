package alphapuzzle.crop

import io.circe.Codec
import org.opencv.core.Point

case class Quadrilateral(
    topLeft: Coordinate,
    topRight: Coordinate,
    bottomLeft: Coordinate,
    bottomRight: Coordinate
) derives Codec:

  private lazy val rightMostX: Int = Math.max(topRight.x, bottomRight.x)
  private lazy val leftMostX: Int = Math.min(topLeft.x, bottomLeft.x)
  private lazy val bottomMostY: Int = Math.max(bottomLeft.y, bottomRight.y)
  private lazy val topMostY: Int = Math.min(topLeft.y, topRight.y)

  lazy val area: Double =
    val (x1, y1, x2, y2, x3, y3, x4, y4) = (
      topLeft.x,
      topLeft.y,
      topRight.x,
      topRight.y,
      bottomRight.x,
      bottomRight.y,
      bottomLeft.x,
      bottomLeft.y
    )
    Math.abs(
      (x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2) + (x3 * y4 - x4 * y3) + (x4 * y1 - x1 * y4)
    ) * 0.5

  lazy val aspectRatio: Double =
    (rightMostX - leftMostX).toDouble / (bottomMostY - topMostY).toDouble

  def toRectangle: Quadrilateral =
    Quadrilateral(
      Coordinate(leftMostX, topMostY),
      Coordinate(rightMostX, topMostY),
      Coordinate(leftMostX, bottomMostY),
      Coordinate(rightMostX, bottomMostY)
    )

  def toCropRectangle: CropRectangle =
    CropRectangle(
      x = leftMostX,
      y = topMostY,
      width = rightMostX - leftMostX,
      height = bottomMostY - topMostY
    )

object Quadrilateral:

  def apply(points: Seq[Point]): Option[Quadrilateral] =
    val mx = points.map(_.x).sum / points.size.toDouble
    val my = points.map(_.y).sum / points.size.toDouble
    def findPointWhere(pred: (Double, Double) => Boolean): Option[Point] =
      points.find(p => pred(p.x, p.y))
    for
      topLeft <- findPointWhere((x, y) => x < mx && y < my)
      topRight <- findPointWhere((x, y) => x > mx && y < my)
      bottomLeft <- findPointWhere((x, y) => x < mx && y > my)
      bottomRight <- findPointWhere((x, y) => x > mx && y > my)
    yield Quadrilateral(
      Coordinate(topLeft),
      Coordinate(topRight),
      Coordinate(bottomLeft),
      Coordinate(bottomRight)
    )

  def join(quadrilaterals: Seq[Quadrilateral]): Option[Quadrilateral] =
    val empty: Option[Quadrilateral] = None
    quadrilaterals.foldLeft(empty): (acc, quadrilateral) =>
      acc
        .map: existingQuadrilateral =>
          val leftMostX =
            Math.min(existingQuadrilateral.leftMostX, quadrilateral.leftMostX)
          val rightMostX = Math
            .max(existingQuadrilateral.rightMostX, quadrilateral.rightMostX)
          val topMostY =
            Math.min(existingQuadrilateral.topMostY, quadrilateral.topMostY)
          val bottomMostY = Math
            .max(existingQuadrilateral.bottomMostY, quadrilateral.bottomMostY)
          Quadrilateral(
            topLeft = Coordinate(leftMostX, topMostY),
            topRight = Coordinate(rightMostX, topMostY),
            bottomLeft = Coordinate(leftMostX, bottomMostY),
            bottomRight = Coordinate(rightMostX, bottomMostY)
          )
        .orElse(Some(quadrilateral))
