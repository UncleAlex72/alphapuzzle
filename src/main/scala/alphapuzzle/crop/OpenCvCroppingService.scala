package alphapuzzle.crop

import alphapuzzle.crop.Image
import alphapuzzle.crop.Image.*
import alphapuzzle.crop.MatOfPoint2f.*
import cats.implicits.*
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.util.ByteString
import org.opencv.core.{MatOfPoint, Point, Scalar, Size}
import org.opencv.imgcodecs.Imgcodecs.*
import org.opencv.imgproc.Imgproc.*

import java.nio.channels.FileChannel
import java.nio.file.{Files, Path, StandardOpenOption}
import scala.annotation.targetName
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Using.Releasable
import scala.util.{Try, Using}

class OpenCvCroppingService(implicit
    val ec: ExecutionContext
) extends CroppingService:

  OpenCvInitialiser.initialise()

  extension (size: Size)
    @targetName("scale")
    def *(scale: Int): Size = new Size(size.width * scale, size.height * scale)

  given Releasable[Path] = (resource: Path) => Files.deleteIfExists(resource)

  override def crop: Flow[ByteString, CropRectangle, NotUsed] =
    val byteStringCollector: Flow[ByteString, ByteString, NotUsed] =
      Flow[ByteString].fold(ByteString.empty)(_ ++ _)
    def find(byteString: ByteString): Try[Quadrilateral] =
      Using.Manager { use =>
        val path =
          use(Files.createTempFile("alphapuzzle-", ".jpg"))
        val out = use(
          FileChannel
            .open(path, StandardOpenOption.WRITE, StandardOpenOption.APPEND)
        )
        byteString.asByteBuffers.foreach: buffer =>
          out.write(buffer)
        findMainGrid(path)
      }.flatten
    byteStringCollector.mapAsync(1): byteString =>
      Future(find(byteString).get.toCropRectangle)

  def findMainGrid(path: Path): Try[Quadrilateral] =
    val originalImage: Image =
      Image.read(path, IMREAD_GRAYSCALE)
    val imageArea = originalImage.width() * originalImage.height()
    val blurredImage = originalImage.bilateralFilter(9, 75, 75)
    val thresholdImages = createThresholdImages(blurredImage)
    val contours = findContours(thresholdImages)
    findMainGridFromContours(imageArea, contours)
      .toRight(new IllegalStateException("Cannot find a main grid"))
      .toTry

  private def createThresholdImages(originalImage: Image): Seq[Image] =
    val N = 5
    val thresh = 250 / N
    Range(0, N).map: l =>
      if (l == 0)
        originalImage.canny(5, thresh, 5).dilate(Image(), new Point(-1, -1))
      else
        val scalar = new Scalar((l + 1).toDouble * 255.toDouble / N.toDouble)
        originalImage.compareGE(scalar)

  private def findContours(images: Seq[Image]): Seq[MatOfPoint] =
    images.flatMap: img =>
      img.findContours(RETR_LIST, CHAIN_APPROX_SIMPLE)

  private def findMainGridFromContours(
      imageArea: Double,
      contours: Seq[MatOfPoint]
  ): Option[Quadrilateral] =
    val FILTER: Seq[Unit] = Seq({})
    val quadrilaterals = for
      contour <- contours
      approx =
        val contour2f = MatOfPoint2f(contour)
        val arcLength = contour2f.arcLength(true) * 0.02
        contour2f.approxPolyDP(arcLength, closed = true)
      _ <- FILTER if approx.length == 4
      contourArea = Math.abs(approx.contourArea())
      isConvex = approx.isContourConvex()
      _ <- FILTER if contourArea > 1000 && isConvex
      maxCosine: Double = Range(2, 5).foldLeft(0.0): (maxCosine, j) =>
        val cosine = Math.abs(approx.angle(j % 4, j - 2, j - 1))
        Math.max(maxCosine, cosine)

      topLeft = approx(0)
      _ <- FILTER if maxCosine < 0.3 & topLeft.x > 3 && topLeft.y > 3
      squareish <- Quadrilateral(approx.toSeq)
    yield squareish
    // take the largest square like polygon
    val squares = quadrilaterals
      .filter: s =>
        val aspectRatio = s.aspectRatio
        aspectRatio > 0.9 && aspectRatio < 1.1
      .map(_.toRectangle)
      .sortBy(s => -s.area)

    squares.headOption.flatMap: largestQuadrilateral =>
      if (largestQuadrilateral.area * 10 < imageArea)
        Quadrilateral.join(squares)
      else
        Some(largestQuadrilateral)
