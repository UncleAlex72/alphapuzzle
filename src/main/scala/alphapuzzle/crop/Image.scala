package alphapuzzle.crop

import java.io.File
import java.nio.file.Path
import java.util

import org.opencv.core._
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import scala.jdk.CollectionConverters._

object Image:
  def polylines(
      img: Mat,
      pts: Iterable[MatOfPoint],
      isClosed: Boolean,
      color: Scalar,
      thickness: Int,
      lineType: Int
  ): Unit =
    Imgproc.polylines(
      img,
      pts.toList.asJava,
      isClosed,
      color,
      thickness,
      lineType
    )

  def read(path: Path, flags: Int): Image =
    Imgcodecs.imread(path.toFile.getAbsolutePath, flags)

  type Image = Mat

  def apply(): Image = new Mat()

  extension [I <: Image](image: I)

    private def transform(trn: Image => Unit): Image =
      val target = Image()
      trn(target)
      target

    private def cloneAndTransform(trn: Image => Unit): Image =
      val newImage = image.clone();
      trn(newImage)
      newImage

    def putText(
        text: String,
        org: Point,
        fontFace: Int,
        fontScale: Double,
        color: Scalar,
        thickness: Int
    ): Unit =
      Imgproc.putText(image, text, org, fontFace, fontScale, color, thickness)

    def mean(): Scalar = Core.mean(image)

    def crop(rect: Rect): Image = new Mat(image, rect)

    def bilateralFilter(
        d: Int,
        sigmaColor: Double,
        sigmaSpace: Double
    ): Image =
      transform(Imgproc.bilateralFilter(image, _, d, sigmaColor, sigmaSpace))

    def write(target: Path): Unit =
      write(target.toFile)

    def write(target: File): Unit =
      Imgcodecs.imwrite(target.getAbsolutePath, image)

    private def compare(threshold: Scalar, operation: Int): Image =
      transform(Core.compare(image, threshold, _, operation))

    def compareGE(threshold: Scalar): Image =
      compare(threshold, Core.CMP_GE)

    def cvtColor(code: Int): Image =
      transform(Imgproc.cvtColor(image, _, code))

    def canny(
        threshold1: Double,
        threshold2: Double,
        apertureSize: Int
    ): Image =
      transform(Imgproc.Canny(image, _, threshold1, threshold2, apertureSize))

    def dilate(kernel: Mat, anchor: Point): Image =
      transform(Imgproc.dilate(image, _, kernel, anchor))

    def rectangle(rec: Rect, color: Scalar, thickness: Int): Image =
      cloneAndTransform(Imgproc.rectangle(_, rec, color, thickness))

    def findContours(mode: Int, method: Int): Seq[MatOfPoint] =
      val contours = new util.ArrayList[MatOfPoint]()
      Imgproc.findContours(image, contours, new Mat(), mode, method)
      contours.asScala.toSeq
