package alphapuzzle.grid

import alphapuzzle.visionai.VisionAiResponse
import io.circe.parser
import org.scalatest.matchers.should
import org.scalatest.wordspec.AnyWordSpec

import javax.imageio.ImageIO
import scala.io.Source
import scala.util.Using

class GridMatcherImplSpec extends AnyWordSpec with should.Matchers {

  "The grid matcher spec" should {
    "correctly match a grid from an image" in {
      val puzzleName = "ds-3"
      val (width, height): (Int, Int) = {
        Using(
          classOf[GridMatcherImplSpec].getClassLoader.getResourceAsStream(
            s"images/cropped/$puzzleName.jpg"
          )
        ) { in =>
          val image = ImageIO.read(in)
          (image.getWidth, image.getHeight)
        }
      }.get
      val vision: VisionAiResponse = {
        Using(
          classOf[GridMatcherImplSpec].getClassLoader.getResourceAsStream(
            s"vision/$puzzleName.json"
          )
        ) { in =>
          val content = Source.fromInputStream(in).getLines().mkString("\n")
          for {
            json <- parser.parse(content).toTry
            vision <- json.as[VisionAiResponse].toTry
          } yield {
            vision
          }
        }
      }.flatten.get
      val puzzleEntries: Seq[PuzzleEntry] = for {
        textAnnotation <- vision.textAnnotations
        number <- textAnnotation.parse
      } yield {
        val topLeft = textAnnotation.boundingPoly.topLeft
        PuzzleEntry(topLeft.x, topLeft.y, number)
      }
      val grid: Map[(Int, Int), Int] =
        new GridMatcherImpl()
          .matchToGrid(puzzleEntries, Dimension(width = width, height = height))

      val line = Range(0, 52).map(_ => "-").mkString
      def createRow(row: Int): String = {
        val contents = for {
          col <- Range(0, 13)
        } yield {
          grid.get((col, row)) match {
            case Some(n) => n.toString.padTo(3, ' ')
            case None    => "   "
          }
        }
        contents.mkString("|")
      }
      val gridString = Range(0, 13).map(createRow).mkString(s"\n$line\n")
      println(gridString)
    }
  }
}
