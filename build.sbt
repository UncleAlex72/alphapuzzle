import sbtrelease.ReleaseStateTransformations._

val _scalaVersion = "3.6.2"

name := "alphapuzzle"
scalaVersion := _scalaVersion
resolvers += "Atlassian public repository" at "https://packages.atlassian.com/maven-public/"
libraryDependencies ++= Seq(
  "org.openpnp" % "opencv" % "4.9.0-0",
  "com.google.api-client" % "google-api-client" % "2.7.1",
  "com.google.oauth-client" % "google-oauth-client-jetty" % "1.37.0",
  "com.google.cloud" % "google-cloud-vision" % "3.53.0",
  "uk.co.unclealex" %% "futures" % "2.0.0",
  "ch.qos.logback" % "logback-classic" % "1.5.15",
  "uk.co.unclealex" %% "pekko-oidc-google" % "5.0.1",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
  "commons-io" % "commons-io" % "2.18.0",
  "net.sf.jmimemagic" % "jmimemagic" % "0.1.5",
) ++ Seq(
  "uk.co.unclealex" %% "thank-you-for-the-days" % "2.0.0",
  "org.scalatest" %% "scalatest" % "3.2.19"
).map(_ % Test)
dockerBaseImage := "adoptopenjdk/openjdk11-openj9:alpine"
dockerExposedPorts := Seq(9000)
maintainer := "Alex Jones <alex.jones@unclealex.co.uk>"
dockerRepository := Some("unclealex72")
packageName := "alphapuzzle"
dockerUpdateLatest := true
Universal / javaOptions ++= Seq(
  "pidfile.path" -> "/dev/null",
  "java.awt.headless" -> "true"
).map {
  case (k, v) =>
    s"-D$k=$v"
}

enablePlugins(DockerPlugin, JavaAppPackaging, AshScriptPlugin)

Compile / sourceGenerators += Def.task {
  val file = (Compile / sourceManaged).value / "version" / "Version.scala"
  IO.write(file,
           s"""
       |package version
       |
       |object Version:
       |  val version: String = "${version.value}"
       |""".stripMargin.trim)
  Seq(file)
}.taskValue

// Releases

releaseProcess := Seq[ReleaseStep](
  releaseStepCommand("scalafmtCheckAll"),
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand(
    "stage"
  ), // : ReleaseStep, build server docker image.
  releaseStepCommand(
    "docker:publish"
  ), // : ReleaseStep, build server docker image.
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
Global / onChangedBuildSource := ReloadOnSourceChanges
